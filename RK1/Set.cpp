#include "Set.hpp"

Set::Set() {
    data = new int[0];
    len = 0;
}

Set::Set(const Set& set) {
    len = set.len;
    data = new int[len];

    for (int i = 0; i < len; i++) {
        data[i] = set.data[i];
    }
}

void Set::add(int a) {
    if (is_exist(a)) {
        return;
    }
    int buff[len];
    
    for (int i = 0; i < len; i++) {
        buff[i] = data[i];
    }

    delete[] data;
    data = new int[++len];

    for (int i = 0; i < len - 1; i++) {
        data[i] = buff[i];
    }

    data[len - 1] = a;
}

Set& Set::operator=(const Set& right) {
    if (this == &right) {
        return *this;
    }
    delete[] data;
    

    len = right.len;
    data = new int[len];

    for (int i = 0; i < len; i++) {
        data[i] = right.data[i];
    }

    return *this;
}


int& Set::operator[](size_t i) const {
    if (i < len) {
        return data[i];
    } else {
        exit(-1);
    }
}

Set operator+(const Set& left, const Set& right) {
    Set res(left);
    for (size_t i = 0; i < right.get_len(); i++) {
        res.add(right[i]);
    }

    return res;
}

size_t Set::get_len() const {
    return len;
}

Set operator-(const Set& left, const Set& right) {
    Set res;

    for (size_t i = 0; i < left.get_len(); i++) {
        if (!right.is_exist(left[i])) {
            res.add(left[i]);
        }
    }

    return res;
}

Set operator*(const Set& left, const Set& right) {
    Set res;

    for (size_t i = 0; i < left.get_len(); i++) {
        if (right.is_exist(left[i])) {
            res.add(left[i]);
        }
    }

    return res;
}

std::ostream& operator<<(std::ostream& os, const Set& set) {
    for (int i = 0; i < set.len; i++) {
        os << set.data[i] << " ";
    }

    return os;
}

Set::~Set() {
    if (data) {
        delete[] data;
    }
}

bool Set::is_exist(int a) const {
    for (int i = 0; i < len; i++) {
        if (data[i] == a) {
            return true;
        }
    }
    return false;
}
