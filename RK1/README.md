Номер варианта берется из номера в таблице (нечетный номер — вариант 1, четный номер — вариант 2)
https://docs.google.com/spreadsheets/d/1QQTTOdkjZOi5t..
(по ссылкам справка по реализации операций, если что-то забыли; советую список перегрузок, методов и справку записать на листочек)

Пользоваться std::vector и std::set нельзя!

1) класс N-мерный Вектор, деструктор/конструктор копирования/оператор присваивания, метод заполнения вектора случайными числами, перегрузка + - * (для 2 векторов - скалярное произведение, вектор и число / друж. число и вектор - просто масштабирование вектора), перегрузка < <, перегрузка []
http://www.math.mrsu.ru/text/courses/method/n-mernie_..

2) класс N-мерное числовое множество (Set), 
деструктор/
конструктор копирования/
оператор присваивания, 
метод добавление элемента в множество (с проверкой дубликатов, их не добавляем), 
перегрузка + (объединение множеств), 
* (пересечение множеств), 
перегрузка < <, 
перегрузка - (разность множеств, A - B - все элементы из A, которых нет в B)
http://www.bymath.net/studyguide/sets/sec/sets2.htm

Почта для отправки РК: ovzbmstu@gmail.com
Ссылка для загрузки видео: https://forms.gle/2D7jkcRVzwQhfrnA6 (если есть возможность, загружайте на свои диски/облака, т.к. у бесплатного гугл.диска ограничения по памяти, а вас очень много ;( )