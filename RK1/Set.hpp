#pragma once

#include <iostream>

class Set {
 public:
    Set();
    Set(const Set& set);
    bool is_exist(int a) const;

    void add(int a);

    size_t get_len() const;

    Set& operator=(const Set& right);

    int& operator[](size_t) const;

    friend Set operator+(const Set& left, const Set& right);

    friend Set operator-(const Set& left, const Set& right);

    friend Set operator*(const Set& left, const Set& right);

    friend std::ostream& operator<<(std::ostream& os, const Set& set);

    ~Set();
 private:

    size_t len;

    int* data;
};
