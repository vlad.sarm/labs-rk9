#include "Fraction.h"

Fraction::Fraction(int num, int den) {
    if (den < 0){
        num = -num;
        den = abs(den);
    }
    this->num = num;
    this->den = den;
    simplify();
}

int Fraction::get_num(){
    return this->num;
}
int Fraction::get_den(){
    return this->den;
}

void Fraction::set_num(int num){
    this->num = num;
    simplify();
}
void Fraction::set_den(int den){
    if (den < 0){
        num = -num;
        den = abs(den);
    }
    this->den = den;
    simplify();
}

Fraction Fraction::operator+(const Fraction& right) const {
    if(den != right.den)
        return Fraction(num*right.den + right.num*num, den*right.den);
    else
        return Fraction(num + right.num, den);
}
Fraction Fraction::operator-(const Fraction& right) const {
    if(den != right.den)
        return Fraction(num*right.den - right.num*num, den*right.den);
    else
        return Fraction(num - right.num, den);
}
Fraction Fraction::operator*(const Fraction& right) const {
    return Fraction(num*right.num, den*right.den);
}
Fraction Fraction::operator/(const Fraction& right) const {
    return Fraction(num*right.den, den*right.num);
}

Fraction Fraction::operator-() const {
    return Fraction(-num, den);
}

std::ostream& operator<<(std::ostream& os, const Fraction& f) {
    os << f.num << '/' << f.den << ' ';
    return os;
}

Fraction& Fraction::operator+=(const Fraction& right) {
    if(den != right.den) {
        num*=right.den;
        num+=right.num*den;
        den*=right.den;
    }
    else
        num+=right.num;
    simplify();
    return *this;
}
Fraction& Fraction::operator-=(const Fraction& right) {
    if(den != right.den) {
        num*=right.den;
        num-=right.num*den;
        den*=right.den;
    }
    else
        num-=right.num;
    simplify();
    return *this;
}
Fraction& Fraction::operator*=(const Fraction& right) {
    num*=right.num;
    den*=right.den;
    simplify();
    return *this;
}
Fraction& Fraction::operator/=(const Fraction& right) {
    if(right.num < 0)
        num*=-right.den;
    else
        num*=right.den;
    den*=abs(right.num);
    simplify();
    return *this;
}

bool operator==(const Fraction& left, const Fraction& right) {
    return (left.num == right.num)&&(left.den == right.den)?true:false;
}
bool operator!=(const Fraction& left, const Fraction& right) {
    return (left.num != right.num)&&(left.den != right.den)?true:false;
}
bool operator<(const Fraction& left, const Fraction& right) {
    if(left.den != right.den)
        return (left.num*right.den < right.num*left.den)?true:false;
    else
        return (left.num < right.num)?true:false;
}
bool operator>(const Fraction& left, const Fraction& right) {
    if(left.den != right.den)
        return (left.num*right.den > right.num*left.den)?true:false;
    else
        return (left.num > right.num)?true:false;
}
bool operator<=(const Fraction& left, const Fraction& right) {
    if(left.den != right.den)
        return (left.num*right.den <= right.num*left.den)?true:false;
    else
        return (left.num <= right.num)?true:false;
}
bool operator>=(const Fraction& left, const Fraction& right) {
    if(left.den != right.den)
        return (left.num*right.den >= right.num*left.den)?true:false;
    else
        return (left.num >= right.num)?true:false;
}

Fraction Fraction::operator+(const int& right) const {
    return Fraction(num + right*den, den);
}
Fraction Fraction::operator-(const int& right) const {
    return Fraction(num - right*den, den);
}
Fraction Fraction::operator*(const int& right) const {
    return Fraction(num * right, den);
}
Fraction Fraction::operator/(const int& right) const {
    return Fraction(num, den * right);
}

Fraction operator+(const int& left, const Fraction& right){
    return Fraction(right.num + left*right.den, right.den);
}
Fraction operator-(const int& left, const Fraction& right) {
    return Fraction(right.num - left*right.den, right.den);
}
Fraction operator*(const int& left, const Fraction& right) {
    return Fraction(right.num * left, right.den);
}
Fraction operator/(const int& left, const Fraction& right) {
    return Fraction(right.num, right.den * left);
}

int Fraction::euclid(int a, int b){
  if (b == 0) return a;
  else return euclid(b, a % b);
}

void Fraction::simplify(){
    int nod = euclid(abs(num), den);
    num/=nod;
    den/=nod;
}
