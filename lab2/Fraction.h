#ifndef FRACTION_H
#define FRACTION_H
#include <ostream>

class Fraction {
private:
    int num, den;
public:
    Fraction(int num = 1, int den = 1);

    int get_num();
    int get_den();

    void set_num(int num);
    void set_den(int den);

    Fraction operator+(const Fraction& right) const;
    Fraction operator-(const Fraction& right) const;
    Fraction operator*(const Fraction& right) const;
    Fraction operator/(const Fraction& right) const;

    Fraction operator-() const;

    friend std::ostream& operator<<(std::ostream& os, const Fraction& f);

    Fraction& operator+=(const Fraction& right);
    Fraction& operator-=(const Fraction& right);
    Fraction& operator*=(const Fraction& right);
    Fraction& operator/=(const Fraction& right);

    friend bool operator==(const Fraction& left, const Fraction& right);
    friend bool operator!=(const Fraction& left, const Fraction& right);
    friend bool operator<(const Fraction& left, const Fraction& right);
    friend bool operator>(const Fraction& left, const Fraction& right);
    friend bool operator<=(const Fraction& left, const Fraction& right);
    friend bool operator>=(const Fraction& left, const Fraction& right);

    Fraction operator+(const int& right) const;
    Fraction operator-(const int& right) const;
    Fraction operator*(const int& right) const;
    Fraction operator/(const int& right) const;

    friend Fraction operator+(const int& left, const Fraction& right);
    friend Fraction operator-(const int& left, const Fraction& right);
    friend Fraction operator*(const int& left, const Fraction& right);
    friend Fraction operator/(const int& left, const Fraction& right);
    
    int euclid(int a, int b);
    void simplify();
};

#endif // FRACTION_H
