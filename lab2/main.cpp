#include <iostream>

#include "Fraction.cpp"

using namespace std;

int main(){
    Fraction f1(4,3), f2(16,12), f3(4, -3), f4(-16, 12);
    cout << f1 << f2 << f3 << f4 << endl;
    cout << (f1 == f2) << (f1 == f3) << (f1 < f2) << endl;
    cout << f1+f2 << f1-f2 << f1*f2 << f1/f2 << endl;
    cout << -f1 << -f3 << endl;
    f1+=f4;
    cout << f1 << endl;
    Fraction f5(3,2);
    f5/=f4;
    cout << f5 << endl;
    cout << f5 << f5 + 2 << f5/4;
    return 0;
}
