#pragma once


class Point {
 public:
    Point(double x = 0, double y = 0);
    Point(const Point& p);
    ~Point() = default;

    Point& operator=(const Point& right);
    friend const Point operator+(const Point& left, const Point& right);
    friend Point& operator+=(Point& left, const Point& right);
    friend const Point operator*(const Point& left, const double& right);
    

    friend double distance(const Point& first, const Point& second);
 private:
    double x;
    double y;
};
