#include "Polynome.h"


Polynome::Polynome(const Point& p0, const Point& p1, const Point& p2) :
    p0(p0), p1(p1), p2(p2) {}

Point Polynome::f(double t) const {
    return (p2 * t * t + p1 * t + p0);
}