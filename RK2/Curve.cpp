#include "Curve.h"


double Curve::length(double t0, double t1) const {
    double res = 0;
    while (distance(f(t0), f(t1)) >= delta) {
        res += distance(f(t0), f(t1));
        t0 += delta;
    }
    res += distance(f(t0), f(t1));
    return res;
}