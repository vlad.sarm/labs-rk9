#include "Point.h"
#include <cmath>

Point::Point(double x, double y) : x(x), y(y) {}

Point::Point(const Point& p) {
    x = p.x;
    y = p.y;
}

Point& Point::operator=(const Point& right) {
    if (this == &right) {
        return *this;
    }
    x = right.x;
    y = right.y;
    return *this;
}

const Point operator+(const Point& left, const Point& right) {
    return Point(left.x + right.x, left.y + right.y);
}

Point& operator+=(Point& left, const Point& right) {
    left.x += right.x;
    left.y += right.y;

    return left;
}

const Point operator*(const Point& left, const double& right) {
    return Point(left.x*right, left.y*right);
}


double distance(const Point& first, const Point& second) {
    return sqrt(pow(first.x - second.x, 2) + pow(first.y - second.y, 2));
}