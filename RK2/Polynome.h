#pragma once
#include "Curve.h"
#include "Point.h"


class Polynome : public Curve {
 public:
    Polynome(const Point& p0, const Point& p1, const Point& p2);

    virtual Point f(double t) const override;
 private:
    Point p0;
    Point p1;
    Point p2;
};
