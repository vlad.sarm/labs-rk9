#include <iostream>
#include "Polynome.h"


int main() {

    Polynome pol1(Point(0, 0), Point(1, 1),Point(0, 0));
    std::cout << pol1.length(0, 1) << std::endl;
    Polynome pol2(Point(1, 0), Point(0, 1),Point(0, 0));
    std::cout << pol2.length(0, 1) << std::endl;
    Polynome pol3(Point(1, 0), Point(1, 1),Point(1, 0));
    std::cout << pol3.length(0, 1) << std::endl;
    return 0;
}
