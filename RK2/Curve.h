#pragma once
#include "Point.h"

const double delta = 0.01f;

class Curve {
 public:
    virtual ~Curve() = default;
    virtual Point f(double t) const = 0;
    virtual double length(double t0, double t1) const;
};