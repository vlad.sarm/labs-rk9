CC = g++

TARGET = main.out

#lab1
HDRS_DIR_1 = lab1/
SRCS_1 = lab1/main.cpp

lab1: $(SRCS_1)
	$(CC) -Wall -Wextra -Werror -o $(TARGET) $(SRCS_1) -lm

#lab2
HDRS_DIR_2 = lab2/
SRCS_2 = lab2/main.cpp

lab2: $(SRCS_2)
	$(CC) -Wall -Wextra -Werror -o $(TARGET) $(SRCS_2) -lm

#lab4(3)
HDRS_DIR_4 = lab4/include
SRCS_4 = lab4/src/main.cpp \
	lab4/src/My_String.cpp

lab4: $(SRCS_4)
	$(CXX) -std=gnu++17 $(addprefix -I,$(HDRS_DIR_4)) -o $(TARGET) $(CFLAGS) $(SRCS_4)

clean:
	rm -rf $(TARGET)

.PHONY: lab1 lab2 lab4 clean
