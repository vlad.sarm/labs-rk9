#include <iostream>
#include "My_String.h"

using namespace std;
int main() {
    My_String s1(43);
    My_String s2("Hello my world!");
    string s = "aw";
    My_String s3(s);

    cout << s1 << endl << s2 << endl << s3<< endl;
    s3.clear();
    cout << s3.is_empty() << endl;
    cout << s2.substr(0,8) << endl;
    cout << s2.first_word() << endl << s2.last_word() << endl;
    
    cout << s2.is_char_exist('f') << endl << s2.is_substr_exist(My_String("wodr")) << endl;
    s2.subclear(0,5);
    cout << s2.substr(0,5) << endl;
    cout << s2+s1 << endl;
    My_String s4("HELLO ");
    My_String s5("WORLD!");
    s4 += s5;
    cout << s4 << endl;
    My_String ss("aa");
    My_String sb("ab");
    cout << (ss == sb) << endl << (ss != sb) << endl << (ss < sb) << endl << (ss > sb) << endl << (ss <= sb) << endl << (ss>= sb) << endl;
    cout << "bb" + sb << endl;
    const char* sc = "ab";
    cout << (sc == ss) << endl << (ss != sc) << endl << (sc < ss) << endl << (ss > sc) << endl << (sc <= ss) << endl << (ss>= sc) << endl;
    return 0;
}
