#include "My_String.h"
#include <iostream>
#include <cstring>
#include <string>
#include <cstdlib>


My_String::My_String() {
    data = new char[1];
    data = 0;
}

My_String::My_String(const My_String& str) {
    *this = My_String(str.data);
}

My_String::My_String(int num) {
    int len = 1;
    int n = num;
    while (n) {
        n = n / 10;
        len++;
    }
    data = new char(len);
    sprintf(data, "%d", num);
}

My_String::My_String(const char* str) {
    data = new char[strlen(str) + 1];
    sprintf(data, "%s", str);
}

My_String::My_String(std::string& str) {
    data = new char[str.size() + 1];
    sprintf(data, "%s", str.c_str());
}

bool My_String::is_empty() {
    if (size() == 0) {
        return true;
    }
    return false;
}

size_t My_String::size() const {
    return strlen(data);
}

void My_String::clear() {
    if (data) {
        delete[] data;
    }
    data = new char[1];
    strcpy(data, "");
}

My_String My_String::substr(size_t left, size_t right) {
    My_String res;
    res.data = new char[size() + 1];
    size_t u = 0;
    for (size_t i = left; i <= right; i++) {
        res.data[u++] = data[i]; 
    }
    return res;
}

void My_String::subclear(size_t left, size_t right) {
    My_String res;
    res.data = new char[size()+1];
    size_t i, u;
    i = u = 0;
    for (; i < size() + 1; i++) {
        if (i < left || i > right) {
            res.data[u++] = data[i];
        }
    }
    if (data) {
        delete[] data;
    }
    data = new char[res.size() + 1];
    sprintf(data, "%s", res.data);
}

My_String My_String::first_word() {
    My_String res;
    res.data = new char[ size() + 1];
    for (size_t i = 0; i < size() + 1 && data[i] != ' '; i++) {
        res.data[i] = data[i];
    }
    return res;
}

My_String My_String::last_word() {
    My_String res;
    res.data = new char[size() + 1];
    int i = size() - 1;
    for ( ; data[i] != ' '; i--) {
        if (!i) {
            return *this;
        }
    }
    strncpy(res.data, &data[i+1], size()-i);
    return res;
}

const char* My_String::get_data() const {
    return data;
}

bool My_String::is_char_exist(char c) {
    for (int i = 0; i < size(); i++) {
        if (data[i] == c) {
            return true;
        }
    }
    return false;
}
bool My_String::is_substr_exist(My_String str) {
    int u = 0;
    for (int i = 0; i < size(); i++) {
        if (data[i] == str[u]) {
            ++u;
        } else {
            u = 0;
        }
        if (u == str.size()) {
            return true;
        }
    }
    return false;
}


std::ostream& operator<<(std::ostream& os, const My_String& str) {
    return os << str.data;
}

My_String& My_String::operator+=(const My_String& str) {
    int len = size() + str.size() + 1;
    My_String res;
    res.data = new char[len];
    sprintf(res.data, "%s%s", data, str.data);
    if (data) {
        delete[] data;
    }
    data = new char(len);
    sprintf(data, "%s", res.data);
    return *this;
}

My_String& My_String::operator=(const My_String& right) {
    if (data == right.data) {
        return *this;
    }
    if (data) {
        delete[] data;
    }
    data = new char(right.size() + 1);
    sprintf(data, "%s", right.data);

    return *this;
}

bool My_String::operator==(const My_String& right) const {
    if (!strcmp(data, right.data)) {
        return true;
    }
    return false;
}

bool My_String::operator!=(const My_String& right) const {
    if (!strcmp(data, right.data)) {
        return false;
    }
    return true;
}

bool My_String::operator<(const My_String& right) const {
    if (strcmp(data, right.data) < 0) {
        return true;
    }
    return false;
}

bool My_String::operator>(const My_String& right) const {
    if (strcmp(data, right.data) > 0) {
        return true;
    }
    return false;
}

bool My_String::operator<=(const My_String& right) const {
    if (strcmp(data, right.data) <= 0) {
        return true;
    }
    return false;
}

bool My_String::operator>=(const My_String& right) const {
    if (strcmp(data, right.data) >= 0) {
        return true;
    }
    return false;
}

char My_String::operator[](size_t i) const {
    return data[i];
}

char& My_String::operator[](size_t i) {
    return data[i];
}

My_String operator+(const char* str, const My_String right) {
    My_String res;
    res.data = new char[right.size() + strlen(str) + 1];
    sprintf(res.data, "%s%s", str, right.data);
    return res;
}

My_String operator+(const My_String left, const My_String& right) {
    My_String res;
    res.data = new char[left.size() + right.size() + 1];
    sprintf(res.data, "%s%s", left.data, right.data);
    return res;
}

bool operator==(const char* left, const My_String& right) {
    if (!strcmp(left, right.data)) {
        return true;
    }
    return false;
}

bool operator!=(const char* left, const My_String& right) {
    if (!strcmp(left, right.data)) {
        return false;
    }
    return true;
}

bool operator<(const char* left, const My_String& right) {
    if (strcmp(left, right.data) < 0) {
        return true;
    }
    return false;
}

bool operator>(const char* left, const My_String& right) {
    if (strcmp(left, right.data) > 0) {
        return true;
    }
    return false;
}

bool operator<=(const char* left, const My_String& right) {
    if (strcmp(left, right.data) <= 0) {
        return true;
    }
    return false;
}

bool operator>=(const char* left, const My_String& right) {
    if (strcmp(left, right.data) >= 0) {
        return true;
    }
    return false;
}

My_String::~My_String() {
    if (!data) {
        delete[] data;
    }
}