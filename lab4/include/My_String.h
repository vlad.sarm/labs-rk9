#pragma once
#include <string>

class My_String {
 private:
    char* data;
 public:
    My_String(const My_String& str);
    My_String();
    My_String(int num);
    My_String(const char* str);
    My_String(std::string& str);

    const char* get_data() const;

    bool is_empty();
    size_t size() const;
    void clear();

    My_String substr(size_t left, size_t right);
    void subclear(size_t left, size_t right);

    bool is_char_exist(char c);
    bool is_substr_exist(My_String str);

    My_String first_word();
    My_String last_word();

    friend  My_String operator+(const My_String left, const My_String& right);

    friend std::ostream& operator<<(std::ostream& os, const My_String& str);

    My_String& operator+=(const My_String& right);
    
    My_String& operator=(const My_String& right);
    
    bool operator==(const My_String& right) const;
    bool operator!=(const My_String& right) const;
    bool operator<(const My_String& right) const;
    bool operator>(const My_String& right) const;
    bool operator<=(const My_String& right) const;
    bool operator>=(const My_String& right) const;

    char operator[](size_t i) const;
    char& operator[](size_t i);

    friend  My_String operator+(const char* str, const My_String right);
    friend bool operator==(const char* left, const My_String& right);
    friend bool operator!=(const char* left, const My_String& right);
    friend bool operator<(const char* left, const My_String& right);
    friend bool operator>(const char* left, const My_String& right);
    friend bool operator<=(const char* left, const My_String& right);
    friend bool operator>=(const char* left, const My_String& right);

    ~My_String();
};