#include "Point.h"

Point::Point(double x = 0, double y = 0){
    this->x = x;
    this->y = y;
};

double Point::get_x(){
    return x;
}

void Point::set_x(double x){
    this->x = x;
}

double Point::get_y(){
    return y;
}

void Point::set_y(double y){
    this->y = y;
}

void Point::move(double dx, double dy){
    set_x(get_x()+dx);
    set_y(get_y()+dy);
}
double Point::force(Point& second){
    double r = pow((x - second.x), 2) + pow((y-second.y), 2);
    if (r == 0){
        return MAXFLOAT;
    }
    
    return r;
}

Point::~Point(){
    std::cout << "Point destructed!\n";
}