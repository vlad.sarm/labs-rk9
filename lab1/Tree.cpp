#include "Tree.h"
#include "string.h"

Tree::Tree(const char* name){
    age = 0;
    bug = 0;
    this->name = (char*)malloc(sizeof(char)*20);
    strcpy(this->name, name);

    std::cout << name << " is planted!" << std::endl;
}

int Tree::get_bug(){
    return bug;
}

void Tree::grow_up(){
    bug += rand() % 10;
    age++;
}