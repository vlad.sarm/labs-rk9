#pragma once

#include <iostream>
#include <cmath>

class Point
{
private:
    double x;
    double y;

public:

    double q;

    Point(double x, double y);

    double get_x();
    void set_x(double x);
    double get_y();
    void set_y(double y);

    double force(Point& second);

    void move(double dx, double dy);

    ~Point();
};
