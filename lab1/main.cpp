#include <cstdlib>

#include "Tree.cpp"

using namespace std;

int main(){
    Tree t1("John"), t2("Sam");
    t1.grow_up();
    t1.grow_up();
    t1.grow_up();
    t2.grow_up();

    cout << t1.name << " : " << t1.get_bug() << endl;
    cout << t2.name << " : " << t2.get_bug() << endl;

    return 0;
}
