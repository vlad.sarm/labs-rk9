#pragma once

#include <iostream>
#include <cmath>

class Tree {
private:
    int bug;
public:
    int age;
    char* name;

    Tree(const char* name);

    int get_bug();

    void grow_up();

    ~Tree(){
        std::cout << "I'm dying!" << std::endl;
    }
};